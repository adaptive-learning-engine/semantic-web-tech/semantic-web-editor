# Semantic Web Editor

**WORK IN PROGRESS - partially hardcoded**

This repository contains the code of a semantic web editor created as part of the [KIWI](https://www.htw-berlin.de/forschung/online-forschungskatalog/projekte/projekt/?eid=3125) project. Currently this editor can connect to a CASS server and display as well as edit competencies.

> Little disclaimer: Please note that the code was developed as part of a scientific project, where the focus was on research and experimentation rather than the usual quality standards of a fully-fledged product code base.

## Dependencies

- [node.js](https://nodejs.org/en) version between 16 and 18 (higher versions are currently not supported)
- [yarn](https://yarnpkg.com/getting-started/install) package manager

## Installation

1. Checkout this repository, make sure to recurse submodules
2. Execute `yarn install` in the repository directory

## Running / Building

All necessary npm/yarn commands can be found in the main [package.json](./package.json).

Most important commands are:

- `yarn start:semantic-web-editor` - starts the dev server of the VLE

## IDE

### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/). It is advised to install the respective extensions in your IDE of choice.

### Linting

This package uses a prettier configuration to set a style format for all source code files and an eslint configuration for linting the typescript code. These configurations are part of the `@adlete/dev-config` package. It is advised to install the respective extensions in your IDE of choice.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).

Since this is a monorepo, make sure to use the package name in your commit messages, e.g. `FIX(vle-core): fixed foo`.

## Common problems

### Adding new packages

Don't use `yarn install *package-name*`, as this repository is a yarn workspace. Installing a package this way, will only add it to the root `package.json`, which is most likely not what you want. Instead, simply add the respective package definitions to the `package.json` of the package that it is needed in and call `yarn install` afterwards in the root folder.

### Problems with Typescript types

Especially after installing new packages, you should restart your IDE so it picks up the new types.

### Duplicate Typescript types

Sometimes multiple version of the same type package, e.g. `@types/react`, are installed, resulting in typescript errors. You can install and execute the [yarn-deduplicate](https://www.npmjs.com/package/yarn-deduplicate) package, followed by a call of `yarn install`, to remove such duplicate packages.

If this does not help, add a [version resolution](https://classic.yarnpkg.com/lang/en/docs/selective-version-resolutions/) to the root `package.json` and call `yarn install` again.

### Vite: Packages from this Monorepo

If Vite should treat packages from this monorepo as source packages (e.g. for rebuilding / hot module reloading), then add the according packages to the list of aliases in the according `vite.conf.js`.

### Weird problems

- Try deleting the `node_modules/.vite` directory of the frontend package that uses Vite.
