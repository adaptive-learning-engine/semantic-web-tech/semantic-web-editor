import { useContext } from 'react';

import { PluginManager } from '@fki/plugin-system/PluginManager';
import { IPlugin } from '@fki/plugin-system/specs';

import { IEditorPlugins } from '../plugins/plugins';

import { AppCore } from './AppCore';
import { AppCoreContext } from './AppCoreContext';

export function useAppCore(): AppCore {
  return useContext(AppCoreContext);
}

export function usePluginManager(): PluginManager<IEditorPlugins> {
  const core = useContext(AppCoreContext);
  return core.plugins;
}

export function usePlugin<
  TPlugins extends { [Property in keyof TPlugins]: IPlugin<TPlugins> } = IEditorPlugins,
  K extends keyof TPlugins = keyof TPlugins,
>(id: K): TPlugins[K] {
  const core = useContext(AppCoreContext);
  return core.plugins.get(id as keyof IEditorPlugins) as unknown as TPlugins[K];
}
