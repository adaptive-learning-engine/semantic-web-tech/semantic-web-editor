import 'typeface-roboto';

import { Box, CircularProgress, CssBaseline } from '@mui/material';
import * as React from 'react';
import { ReactElement, useEffect, useMemo } from 'react';

import { useResettableState } from '@adlete-vle/utils/hooks';

import { StyledFlexibleLayout } from '@fki/editor-core/theme/StyledFlexibleLayout';

import { CustomThemeProvider } from '../plugins/theme/hooks';

import { AppCore } from './AppCore';
import { AppCoreContext } from './AppCoreContext';
import { Menu } from './Menu';

interface IAppUIProps {
  core: AppCore;
}

function AppUI({ core }: IAppUIProps): ReactElement {
  const contextMan = core.plugins.get('contextManager');
  const layoutMan = core.plugins.get('layoutManager');
  const content = (
    <CustomThemeProvider>
      <CssBaseline />
      <Box display="flex" flexDirection="column" style={{ height: '100%' }}>
        <Box>
          <Menu />
        </Box>
        <Box display="flex" flexGrow="1" style={{ position: 'relative' }}>
          <StyledFlexibleLayout layoutManager={layoutMan} />
        </Box>
      </Box>
    </CustomThemeProvider>
  );

  const wrapped = contextMan.render(content);

  return wrapped;
}

export function App(): ReactElement {
  const core = useMemo(() => new AppCore(), []);

  // using resettable state becaues of hot module reloading
  const [isInitialized, setIsInitialized] = useResettableState(() => false, [core]);

  useEffect(() => {
    async function inner() {
      await core.start();
      setIsInitialized(true);
    }
    inner();
  }, [core]);

  // only render application when project-manager was properly initialized
  let content = null;
  if (isInitialized) {
    content = <AppUI core={core} />;
  } else {
    content = (
      <div>
        fooo
        <CircularProgress />
      </div>
    );
  }

  return <AppCoreContext.Provider value={core}>{content}</AppCoreContext.Provider>;
}
