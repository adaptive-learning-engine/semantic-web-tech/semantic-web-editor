import { LayoutManagerMeta, LayoutManagerPlugin } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import { ThemeManagerMeta, ThemeManagerPlugin } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import type { IPluginFactory, IPluginMeta, Plugins } from '@fki/plugin-system/specs';

import { ContextManagerMeta, ContextManagerPlugin } from './ContextManager';
import { CompetencyEditorMeta, CompetencyEditorPlugin } from './competency-editor/CompetencyEditorPlugin';
import { BUILTIN_LAYOUTS } from './layout/builtinLayouts';
import { IEditorPlugins } from './plugins';

// helper function to create non-async factories
function createInstantFactory<TPlugins extends Plugins<TPlugins>, K extends keyof TPlugins>(
  meta: IPluginMeta<keyof TPlugins, K>,
  create: () => TPlugins[K]
): IPluginFactory<TPlugins, K> {
  return {
    meta,
    create: () => Promise.resolve(create()),
  };
}

export const FACTORIES: IPluginFactory<IEditorPlugins, keyof IEditorPlugins>[] = [
  createInstantFactory(ContextManagerMeta, () => new ContextManagerPlugin()),
  createInstantFactory(LayoutManagerMeta, () => new LayoutManagerPlugin(BUILTIN_LAYOUTS)),
  createInstantFactory(ThemeManagerMeta, () => new ThemeManagerPlugin()),
  createInstantFactory(CompetencyEditorMeta, () => new CompetencyEditorPlugin()),
];
