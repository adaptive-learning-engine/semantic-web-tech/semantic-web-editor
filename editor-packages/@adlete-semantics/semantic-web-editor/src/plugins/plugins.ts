import type { IWithLayoutManager } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import type { IWithThemeManager } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import type { IWithAppCore } from '../app/AppCore';

import { IWithContextManager } from './ContextManager';
import { IWithCompetencyEditor } from './competency-editor/CompetencyEditorPlugin';

export interface IEditorPlugins extends IWithAppCore, IWithContextManager, IWithLayoutManager, IWithThemeManager, IWithCompetencyEditor {}
