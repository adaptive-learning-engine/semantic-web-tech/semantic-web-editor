import React from 'react';

import { EditorPanelController } from './EditorPanelController';
import { CreateCompetencyInFrameworkButton } from './providers/cass/components/CreateCompetencyInFrameworkButton';
import { CreateRelationInFrameworkButton } from './providers/cass/components/CreateRelationInFrameworkButton';
import { RemoveCompetencyFromFrameworkButton } from './providers/cass/components/RemoveCompetencyFromFrameworkButton';
import { RemoveRelationFromFrameworkButton } from './providers/cass/components/RemoveRelationFromFrameworkButton';

export interface IEditorPanelToolbar {
  editor: EditorPanelController;
}

export function EditorPanelToolbar(props: IEditorPanelToolbar): React.ReactElement {
  return (
    <div>
      <CreateCompetencyInFrameworkButton editor={props.editor} />
      <CreateRelationInFrameworkButton editor={props.editor} />
      <RemoveCompetencyFromFrameworkButton editor={props.editor} />
      <RemoveRelationFromFrameworkButton editor={props.editor} />
    </div>
  );
}
