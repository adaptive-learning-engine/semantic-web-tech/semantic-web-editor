import { Stack } from '@mui/material';
import { IGraph } from 'graphology-sugiyama/graph';
import { ISugiyamaOptions, positionsFromSugiGraph, sugiyama } from 'graphology-sugiyama/sugiyama';
import { ISugiGraph } from 'graphology-sugiyama/sugiyama/SugiGraph';
import { ISize } from 'graphology-sugiyama/utils/geometric';
import React, { useCallback, useMemo } from 'react';
import ReactFlow, {
  applyEdgeChanges,
  applyNodeChanges,
  Background,
  Controls,
  Edge,
  EdgeChange,
  MarkerType,
  MiniMap,
  Node,
  NodeChange,
  OnSelectionChangeParams,
} from 'reactflow';

import { useForceUpdate } from '@adlete-vle/utils/hooks';

import { ISimpleGraph } from './SimpleGraph';
import { toGraphology } from './graphology-conversion';
import { CassEdge } from './providers/cass/components/CassEdge';
import { CassNode } from './providers/cass/components/CassNode';

const NODE_TYPES = { default: CassNode };
const EDGE_TYPES = { default: CassEdge };

interface IReactFlowGraph<N = unknown> {
  nodes: Node<N>[];
  edges: Edge[];
}

function getNodeSize(): ISize {
  return { width: 400, height: 200 };
}

const SUGIYAMA_OPTIONS: ISugiyamaOptions<unknown> = {
  // childDir: 'in',
  layering: { childDir: 'in', inverseLayers: true },
  getNodeSize,
};

function sugiGraphToReactFlow<N>(sugiGraph: ISugiGraph, graph: IGraph<N>): IReactFlowGraph<N> {
  const positions = positionsFromSugiGraph(sugiGraph);

  const nodes: Node<N>[] = sugiGraph.mapNodes((node) => ({
    id: node,
    data: graph.getNodeAttributes(node),
    position: positions[node],
  }));

  const edges: Edge[] = sugiGraph.mapDirectedEdges((id, attrs, source, target) => ({
    id,
    source,
    target,
    data: graph.getEdgeAttributes(id),
    markerEnd: {
      type: MarkerType.ArrowClosed,
    },
  }));

  return { nodes, edges };
}

export interface ISemanticGraphViewProps {
  graph: ISimpleGraph;

  // TODO: use only id's?
  onSelectionChange: (params: OnSelectionChangeParams) => void;
}

export function SemanticGraphView(props: ISemanticGraphViewProps): React.ReactElement {
  const { graph, onSelectionChange } = props;

  const forceUpdate = useForceUpdate();

  // graph layouting
  const rfGraph = useMemo(() => {
    if (!graph) return { nodes: [], edges: [] };

    const graphConv = toGraphology(graph);
    const sugiGraph = sugiyama(graphConv, SUGIYAMA_OPTIONS);
    return sugiGraphToReactFlow(sugiGraph, graphConv);
  }, [graph]);

  const nodes = rfGraph.nodes;
  const edges = rfGraph.edges;

  // const onConnect = (connection: Connection) => {
  //   console.log(connection);
  // };

  const onNodesChange = useCallback(
    (changes: NodeChange[]) => {
      // TODO Move to fully controlled selection
      rfGraph.nodes = applyNodeChanges(changes, nodes);
      forceUpdate();
    },
    [nodes, rfGraph, forceUpdate]
  );

  const onEdgesChange = useCallback(
    (changes: EdgeChange[]) => {
      // TODO Move to fully controlled selection
      rfGraph.edges = applyEdgeChanges(changes, edges);
      forceUpdate();
    },
    [edges, rfGraph, forceUpdate]
  );

  return (
    // TODO: controlled selection
    <Stack width="100%" height="100%">
      <ReactFlow
        nodes={nodes}
        edges={edges}
        nodeTypes={NODE_TYPES}
        edgeTypes={EDGE_TYPES}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        onSelectionChange={onSelectionChange}
        // onConnect={onConnect}
        multiSelectionKeyCode="Control"
        // selectNodesOnDrag={false}
        nodesDraggable={false}
      >
        <MiniMap />
        <Controls />
        <Background />
      </ReactFlow>
    </Stack>
  );
}
