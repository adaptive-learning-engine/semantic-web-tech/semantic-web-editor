import { useContext } from 'react';

import { useStatefulEvent } from '@adlete-utils/events-react';

import { SemanticWebManager } from '../SemanticWebManager';
import { SemanticWebManagerContext } from '../SemanticWebManagerContext';

import { GraphListManager, IGraphListItem } from './GraphListManager';
import { ProviderManager } from './ProviderManager';

export function useSemanticWebManager(): SemanticWebManager {
  return useContext(SemanticWebManagerContext);
}

export function useGraphListManager(): GraphListManager {
  return useSemanticWebManager().graphList;
}

export function useProviderManager(): ProviderManager {
  return useSemanticWebManager().providers;
}

export function useSelectedGraphListItem(): IGraphListItem {
  const graphListMan = useGraphListManager();
  const graphList = useStatefulEvent(graphListMan.events, 'items');
  const selectedItemId = useStatefulEvent(graphListMan.events, 'selectedItemId');
  return graphList.find((item) => item.id === selectedItemId);
}
