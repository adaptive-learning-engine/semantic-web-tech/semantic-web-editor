import { IStatefulEventEmitterNoEmit, StatefulEventEmitter } from '@adlete-utils/events';

import { CONFIG } from '../config';

const GRAPH_LIST = CONFIG.GRAPH_LIST;

export interface IGraphListItem {
  id: string;
  label: string;
  provider: string;
  active: boolean;
  visible: boolean;
}

export interface IGraphListEvents {
  items: IGraphListItem[];
  selectedItemId: string;
}

export class GraphListManager {
  protected _events: StatefulEventEmitter<IGraphListEvents>;

  public get events(): IStatefulEventEmitterNoEmit<IGraphListEvents> {
    return this._events;
  }

  get selectedItemId(): string {
    return this._events.getState('selectedItemId');
  }

  set selectedItemId(value: string) {
    this._events.emit('selectedItemId', value);
  }

  get items(): IGraphListItem[] {
    return this._events.getState('items');
  }

  public constructor() {
    this._events = new StatefulEventEmitter({ items: [...GRAPH_LIST] });
  }
}
