import type { Attributes } from 'graphology-types';

import { ISemanticWebProvider } from '../ISemanticWebProvider';

// export interface ISemanticWebManager {
//   addProvider(provider: ISemanticWebProvider): void;
//   removeProvider(id: string): void;

//   getProviderIds(): string[];

//   getProvider<
//     TProvider extends ISemanticWebProvider<TNodeAttrs, TEdgeAttrs, TGraphAttrs>,
//     TNodeAttrs extends Attributes = Attributes,
//     TEdgeAttrs extends Attributes = Attributes,
//     TGraphAttrs extends Attributes = Attributes,
//   >(
//     id: string
//   ): TProvider;

//   getProvider<TNodeAttrs extends Attributes, TEdgeAttrs extends Attributes, TGraphAttrs extends Attributes>(
//     id: string
//   ): ISemanticWebProvider<TNodeAttrs, TEdgeAttrs, TGraphAttrs>;
// }

export class ProviderManager {
  protected providers: Record<string, ISemanticWebProvider> = {};

  public addProvider(provider: ISemanticWebProvider): void {
    if (provider.id in this.providers) throw new Error(`Provider with id '${provider.id} already exists!'`);
    this.providers[provider.id] = provider;
  }

  public removeProvider(id: string): void {
    if (!(id in this.providers)) throw new Error(`Provider with id '${id} does not exist!'`);
    delete this.providers[id];
  }

  public getProviderIds(): string[] {
    return Object.keys(this.providers);
  }

  public getProvider<
    TProvider extends ISemanticWebProvider<TNodeAttrs, TEdgeAttrs, TGraphAttrs>,
    TNodeAttrs extends Attributes = Attributes,
    TEdgeAttrs extends Attributes = Attributes,
    TGraphAttrs extends Attributes = Attributes,
  >(id: string): TProvider;
  public getProvider<TNodeAttrs extends Attributes, TEdgeAttrs extends Attributes, TGraphAttrs extends Attributes>(
    id: string
  ): ISemanticWebProvider<TNodeAttrs, TEdgeAttrs, TGraphAttrs, unknown> {
    if (!(id in this.providers)) throw new Error(`Provider with id '${id} does not exist!'`);
    return this.providers[id] as ISemanticWebProvider<TNodeAttrs, TEdgeAttrs, TGraphAttrs>;
  }
}
