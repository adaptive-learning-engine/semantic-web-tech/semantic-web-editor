import { GraphListManager } from './managers/GraphListManager';
import { ProviderManager } from './managers/ProviderManager';

export class SemanticWebManager {
  public providers: ProviderManager;

  public graphList: GraphListManager;

  public constructor() {
    this.providers = new ProviderManager();
    this.graphList = new GraphListManager();
  }
}
