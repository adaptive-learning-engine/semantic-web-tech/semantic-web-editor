import { OnSelectionChangeParams } from 'reactflow';

import { ICassGraphAttributes } from '@adlete-semantics/cass-graph';
import { ICompetency, IRelation } from '@adlete-semantics/cass-json-ld';
import { IStatefulEventEmitterNoEmit, StatefulEventEmitter } from '@adlete-utils/events';

import { SemanticWebManager } from './SemanticWebManager';
import { ISimpleGraph } from './SimpleGraph';
import { GraphListManager, IGraphListItem } from './managers/GraphListManager';
import { CassProvider } from './providers/cass/CassProvider';
import { mergeGraphs } from './providers/cass/graph-utils';

export interface IEditorPanelControllerEvents {
  graph: ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>;
  selection: OnSelectionChangeParams;
}

export class EditorPanelController {
  public semanticWebMan: SemanticWebManager;

  protected graphListMan: GraphListManager;

  protected _events: StatefulEventEmitter<IEditorPanelControllerEvents>;

  public get events(): IStatefulEventEmitterNoEmit<IEditorPanelControllerEvents> {
    return this._events;
  }

  public constructor(semanticWebMan: SemanticWebManager) {
    this.semanticWebMan = semanticWebMan;
    this.graphListMan = semanticWebMan.graphList;
    this._events = new StatefulEventEmitter();
    this.onGraphListChanged = this.onGraphListChanged.bind(this);
    this.graphListMan.events.on('items', this.onGraphListChanged);
  }

  protected setGraph(graph: ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>): void {
    this._events.emit('graph', graph);
  }

  public get graph(): ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes> {
    return this._events.getState('graph');
  }

  public setSelection(selection: OnSelectionChangeParams): void {
    this._events.emit('selection', selection);
  }

  public init() {
    if (this.graphListMan.events.hasState('items')) this.onGraphListChanged(this.graphListMan.events.getState('items'));
  }

  public destroy() {
    this.graphListMan.events.off('items', this.onGraphListChanged);
  }

  protected async onGraphListChanged(items: IGraphListItem[]) {
    const cassProvider = this.semanticWebMan.providers.getProvider<CassProvider>('cass-provider');

    // TODO: handle visibility
    // TODO: handle errors
    const graphs = await Promise.all(items.map((item) => cassProvider.getGraph(item.id)));
    const newGraph = mergeGraphs(graphs);

    this.setGraph(newGraph);
  }
}
