import type { Attributes } from 'graphology-types';

import { ITypedEventEmitter } from '@adlete-utils/events';

import { ISimpleGraph } from './SimpleGraph';

export interface IWithId {
  id: string;
}

export interface IWithSourceAndTarget {
  source: string;
  target: string;
}

export interface IWithAttrs<TAttrs extends Attributes = Attributes> {
  attrs: TAttrs;
}

export type ICreateNodeDatum<TAttrs extends Attributes = Attributes> = Partial<IWithId> & Partial<IWithAttrs<TAttrs>>;
export type ICreateNodeResult<TAttrs extends Attributes = Attributes> = ISuccessResult & IWithAttrs<TAttrs>;
export type ICreateEdgeDatum<TAttrs extends Attributes = Attributes> = Partial<IWithId> &
  Partial<IWithAttrs<TAttrs>> &
  IWithSourceAndTarget;
export type ICreateEdgeResult<TAttrs extends Attributes = Attributes> = ISuccessResult & IWithAttrs<TAttrs> & IWithSourceAndTarget;
export type IUpdateDatum<TAttrs extends Attributes = Attributes> = IWithId & IWithAttrs<TAttrs>;
export type IGetResult<TAttrs extends Attributes = Attributes> = IWithId & IWithAttrs<TAttrs>;
export type IExistsResult = IWithId & { exists: boolean };

export interface ISemanticWebProviderEvents<
  TNodeAttrs extends Attributes = Attributes,
  TEdgeAttrs extends Attributes = Attributes,
  TGraphAttrs extends Attributes = Attributes,
> {
  nodeAdded: [string, TNodeAttrs, string];
  nodeAttributesUpdated: [string, TNodeAttrs, string];
  nodeRemoved: [string, TNodeAttrs, string];
  edgeAdded: [string, TEdgeAttrs, string];
  edgeAttributesUpdated: [string, TEdgeAttrs, string];
  edgeRemoved: [string, TEdgeAttrs, string];
}

export interface IWithContext<V, C> {
  value: V;
  context: C;
}

export interface ISuccessResult {
  id: string;
  success: boolean;
}

export interface ISemanticWebProvider<
  TNodeAttrs extends Attributes = Attributes,
  TEdgeAttrs extends Attributes = Attributes,
  TGraphAttrs extends Attributes = Attributes,
  TOptions = unknown,
> extends ITypedEventEmitter<ISemanticWebProviderEvents<TNodeAttrs, TEdgeAttrs, TGraphAttrs>> {
  id: string;

  // nodes and edges

  createNodes(data: ICreateNodeDatum<TNodeAttrs>[], options?: TOptions): Promise<ICreateNodeResult<TNodeAttrs>[]>;
  nodesExist(ids: string[], options?: TOptions): Promise<IExistsResult[]>;
  getNodes(ids: string[], options?: TOptions): Promise<IGetResult<TNodeAttrs>[]>;
  updateNodes(data: IUpdateDatum<TNodeAttrs>[], options?: TOptions): Promise<ISuccessResult[]>;
  destroyNodes(data: string[], options?: TOptions): Promise<ISuccessResult[]>;

  createEdges(data: ICreateEdgeDatum<TEdgeAttrs>[], options?: TOptions): Promise<ICreateEdgeResult<TEdgeAttrs>[]>;
  edgesExist(ids: string[], options?: TOptions): Promise<IExistsResult[]>;
  getEdges(ids: string[], options?: TOptions): Promise<IGetResult<TEdgeAttrs>[]>;
  updateEdges(data: IUpdateDatum<TEdgeAttrs>[], options?: TOptions): Promise<ISuccessResult[]>;
  destroyEdges(data: string[], options?: TOptions): Promise<ISuccessResult[]>;

  addNodesToGraph(graphId: string, ids: string[], options?: TOptions): Promise<ISuccessResult[]>;
  nodesExistOnGraph(graphId: string, ids: string[], options?: TOptions): Promise<IExistsResult[]>;
  removeNodesFromGraph(graphId: string, ids: string[], options?: TOptions): Promise<ISuccessResult[]>;

  addEdgesToGraph(graphId: string, ids: string[], options?: TOptions): Promise<ISuccessResult[]>;
  edgesExistOnGraph(graphId: string, ids: string[], options?: TOptions): Promise<IExistsResult[]>;
  removeEdgesFromGraph(graphId: string, ids: string[], options?: TOptions): Promise<ISuccessResult[]>;

  getGraph(id: string, options?: TOptions): Promise<ISimpleGraph<TNodeAttrs, TEdgeAttrs, TGraphAttrs>>;
  getGraphIds(options?: TOptions): Promise<string[]>;

  // TODO: addGraph, updateGraph, removeGraph?
  // TODO: context / graph id as part of graph and loose edges?
}
