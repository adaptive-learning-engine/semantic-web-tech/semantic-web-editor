import { useMemo } from 'react';

import { useStatefulEvent } from '@adlete-utils/events-react';

import { EditorPanelController } from '../../EditorPanelController';

export function useSelectedNodes<TNode>(controller: EditorPanelController): TNode[] {
  const selection = useStatefulEvent(controller.events, 'selection');
  return useMemo(() => (selection?.nodes ? selection.nodes.map((node) => node.data) : []), [selection]);
}

export function useSelectedEdges<TEdge>(controller: EditorPanelController): TEdge[] {
  const selection = useStatefulEvent(controller.events, 'selection');
  return useMemo(() => (selection?.edges ? selection.edges.map((edge) => edge.data) : []), [selection]);
}
