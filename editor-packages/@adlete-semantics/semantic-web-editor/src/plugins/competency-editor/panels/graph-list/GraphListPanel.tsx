import { List, ListItemButton } from '@mui/material';
import React from 'react';

import { useStatefulEvent } from '@adlete-utils/events-react';

import { SemanticWebManager } from '../../SemanticWebManager';

export interface IGraphListWindowProps {
  semanticWebMan: SemanticWebManager;
}

// TODO: get proper list of graphs from various providers
export function GraphListPanel(props: IGraphListWindowProps): React.ReactElement {
  const graphListMan = props.semanticWebMan.graphList;
  const items = useStatefulEvent(graphListMan.events, 'items');
  const selectedItemId = useStatefulEvent(graphListMan.events, 'selectedItemId');

  return (
    <List>
      {items.map((item) => (
        <ListItemButton
          key={item.id}
          selected={item.id === selectedItemId}
          onClick={() => {
            graphListMan.selectedItemId = graphListMan.selectedItemId === item.id ? null : item.id;
          }}
        >
          {item.label} ({item.provider})
        </ListItemButton>
      ))}
    </List>
  );
}
