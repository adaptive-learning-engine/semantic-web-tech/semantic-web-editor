import React, { useContext } from 'react';

import { SemanticWebManager } from './SemanticWebManager';

export const SemanticWebManagerContext = React.createContext<SemanticWebManager>(null);

export function useSemanticWebManager(): SemanticWebManager {
  return useContext(SemanticWebManagerContext);
}
