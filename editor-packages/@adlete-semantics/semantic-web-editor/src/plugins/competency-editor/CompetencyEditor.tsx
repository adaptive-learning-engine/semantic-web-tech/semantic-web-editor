import React, { useCallback, useEffect, useMemo } from 'react';
import { OnSelectionChangeParams } from 'reactflow';

import { asString } from '@adlete-semantics/json-ld';
import { useStatefulEvent } from '@adlete-utils/events-react';

import { EditorPanelController } from './EditorPanelController';
import { EditorPanelToolbar } from './EditorPanelToolbar';
import { SemanticGraphView } from './SemanticGraphView';
import { SemanticWebManager } from './SemanticWebManager';

import 'reactflow/dist/style.css';

export interface ICompetencyEditorProps {
  semanticWebMan: SemanticWebManager;
}

export function CompetencyEditor(props: ICompetencyEditorProps): React.ReactElement {
  const { semanticWebMan } = props;

  // TODO: better useRef?
  const controller = useMemo(() => new EditorPanelController(semanticWebMan), []);

  useEffect(() => {
    async function init() {
      await controller.init();
    }
    init();
  }, []);

  const graph = useStatefulEvent(controller.events, 'graph');

  const onSelectionChange = useCallback(
    (params: OnSelectionChangeParams) => {
      controller.setSelection(params);
      console.log('selection nodes', params?.nodes.map((node) => asString(node.data.name)));
      console.log('selection edges', params?.edges.map((node) => asString(node.data.name)));
    },
    [controller]
  );

  // async function addEdgeBetweenSelection() {
  //   if (selection?.nodes.length === 2) {
  //   }
  // }

  return (
    <div style={{ height: '100%' }}>
      <EditorPanelToolbar editor={controller} />
      {/* <AddEdgeDialog source={selection?.nodes?.[0].data} target={selection?.nodes?.[1].data} /> */}
      <SemanticGraphView graph={graph} onSelectionChange={onSelectionChange} />
    </div>
  );
}
