import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import { API as CassAPI } from '@adlete-semantics/cass-api';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from '../ContextManager';

import { CompetencyEditor } from './CompetencyEditor';
import { SemanticWebManager } from './SemanticWebManager';
import { SemanticWebManagerContext } from './SemanticWebManagerContext';
import { CONFIG } from './config';
import { GraphListPanel } from './panels/graph-list/GraphListPanel';
import { CassProvider } from './providers/cass/CassProvider';

export interface IWithCompetencyEditor extends IWithLayoutManager, IWithContextManager {
  competencyEditor: CompetencyEditorPlugin;
}

export const CompetencyEditorMeta: IPluginMeta<keyof IWithCompetencyEditor, 'competencyEditor'> = {
  id: 'competencyEditor',
  dependencies: ['contextManager', 'layoutManager'],
};

export class CompetencyEditorPlugin implements IPlugin<IWithCompetencyEditor> {
  public meta: IPluginMeta<keyof IWithCompetencyEditor, 'competencyEditor'> = CompetencyEditorMeta;

  public async initialize(pluginMan: IPluginManager<IWithCompetencyEditor>): Promise<void> {
    const contextManager = pluginMan.get('contextManager');
    const layoutMan = pluginMan.get('layoutManager');

    const semanticWebMan = new SemanticWebManager();
    contextManager.addContext(SemanticWebManagerContext, semanticWebMan);

    const cassAPI = new CassAPI(CONFIG.CASS_ENDPOINT);
    const cassProvider = new CassProvider(cassAPI);
    semanticWebMan.providers.addProvider(cassProvider);

    layoutMan.panelTypes.add('CompetencyEditor', {
      createPanel: () => <CompetencyEditor semanticWebMan={semanticWebMan} />,
      title: 'Competency Editor',
      icon: AccountTree,
    });
    layoutMan.panelTypes.add('GraphList', {
      createPanel: () => <GraphListPanel semanticWebMan={semanticWebMan} />,
      title: 'Competency Editor',
      icon: AccountTree,
    });
    layoutMan.panelTypes.add('Empty', {
      createPanel: () => <div />,
      title: 'Empty',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
