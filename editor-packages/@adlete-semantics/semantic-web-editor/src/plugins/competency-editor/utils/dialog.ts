import { useMemo, useState } from 'react';

export interface IDialogOpenClose {
  isOpen: boolean;
  setIsOpen: (val: boolean) => void;
  openDialog: () => void;
  closeDialog: () => void;
}

export function useDialogOpenClose(): IDialogOpenClose {
  const [isOpen, setIsOpen] = useState(false);

  const result = useMemo(
    () => ({
      isOpen,
      setIsOpen,
      openDialog: () => {
        setIsOpen(true);
      },
      closeDialog: () => {
        setIsOpen(false);
      },
    }),
    [isOpen, setIsOpen]
  );

  return result;
}
