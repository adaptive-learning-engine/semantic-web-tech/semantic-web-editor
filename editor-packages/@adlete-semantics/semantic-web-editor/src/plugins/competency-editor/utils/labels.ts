export interface IWithIdAndLabel {
  id: string;
  label: string;
}
