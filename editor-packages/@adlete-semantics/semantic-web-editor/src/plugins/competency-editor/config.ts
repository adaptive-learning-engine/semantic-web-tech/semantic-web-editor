import type { IGraphListItem } from './managers/GraphListManager';

export interface ITempConfig {
  CASS_ENDPOINT: string;
  GRAPH_LIST: IGraphListItem[];
}

export const CONFIG: ITempConfig = {
  CASS_ENDPOINT: 'https://cass.fki.htw-berlin.de/api',
  GRAPH_LIST: [
    {
      id: 'e8ac5941-c455-4a59-9658-9916e0bf8a05',
      label: 'Probability Theory (Competencies) ',
      provider: 'cass',
      active: true,
      visible: true,
    },
    {
      id: '93350c0a-c455-45ea-a089-e7624b6e2c79',
      label: 'Probability Theory (Hierarchical Relationships) ',
      provider: 'cass',
      active: true,
      visible: true,
    },
    {
      id: 'bbff3bdd-c455-4955-8451-ac7ae48ee73e',
      label: 'Probability Theory (Prerequisite Relationships) ',
      provider: 'cass',
      active: true,
      visible: true,
    },
    {
      id: 'ec3d2061-c455-4296-aac3-ad1d7fb61b9b',
      label: 'Probability Theory (Progress Relationships) ',
      provider: 'cass',
      active: true,
      visible: true,
    },
  ],
};
