import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React from 'react';

export interface IConfirmDialogProps {
  title: string;
  open: boolean;
  onDeny: () => void;
  onConfirm: () => void;
  children?: React.ReactNode;
}

export function ConfirmDialog(props: IConfirmDialogProps): React.ReactElement {
  const { open, onDeny, onConfirm, title, children } = props;
  return (
    <Dialog onClose={onDeny} open={open}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button onClick={onDeny}>No</Button>
        <Button onClick={onConfirm} autoFocus>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
