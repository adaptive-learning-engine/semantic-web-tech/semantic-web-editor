import { Dialog, DialogContent, DialogTitle } from '@mui/material';
import React from 'react';

export interface IDialogWithTitleProps {
  title: string;
  open: boolean;
  handleClose: () => void;
  children: React.ReactNode;
}

export function DialogWithTitle(props: IDialogWithTitleProps): React.ReactElement {
  const { open, handleClose, title, children } = props;
  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
}
