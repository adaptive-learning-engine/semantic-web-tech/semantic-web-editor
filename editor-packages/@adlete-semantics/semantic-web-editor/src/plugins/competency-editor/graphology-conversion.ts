import Graph from 'graphology';
import { Attributes } from 'graphology-sugiyama/graph';

import type { ISimpleGraph } from './SimpleGraph';

export function toGraphology<
  TNodeAttrs extends Attributes = Attributes,
  TEdgeAttrs extends Attributes = Attributes,
  TGraphAttrs extends Attributes = Attributes,
>(simpleGraph: ISimpleGraph<TNodeAttrs, TEdgeAttrs, TGraphAttrs>): Graph<TNodeAttrs, TEdgeAttrs, TGraphAttrs> {
  const graph = new Graph<TNodeAttrs, TEdgeAttrs, TGraphAttrs>();
  graph.replaceAttributes(simpleGraph.attrs);
  // TODO: Optionally add placeholders if nodes don't
  Object.entries(simpleGraph.nodes).forEach(([id, nodeAttrs]) => graph.addNode(id, nodeAttrs));
  Object.entries(simpleGraph.edges).forEach(([id, edge]) => graph.addDirectedEdgeWithKey(id, edge.source, edge.target, edge.attrs));
  return graph;
}
