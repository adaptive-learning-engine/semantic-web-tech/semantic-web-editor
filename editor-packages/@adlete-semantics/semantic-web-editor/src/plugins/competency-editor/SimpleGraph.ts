import type { Attributes } from 'graphology-types';

export interface IGraphContext {
  id: string;
  providerId: string;
}

export interface ISimpleGraphEdge<TEdgeAttrs> {
  source: string;
  target: string;
  attrs: TEdgeAttrs;
}

export interface ISimpleGraph<
  TNodeAttrs extends Attributes = Attributes,
  TEdgeAttrs extends Attributes = Attributes,
  TGraphAttrs extends Attributes = Attributes,
> {
  context: IGraphContext;
  attrs: TGraphAttrs;
  nodes: Record<string, TNodeAttrs>;
  edges: Record<string, ISimpleGraphEdge<TEdgeAttrs>>;
}

export function createEmptySimpleGraph<
  TNodeAttrs extends Attributes = Attributes,
  TEdgeAttrs extends Attributes = Attributes,
  TGraphAttrs extends Attributes = Attributes,
>(context: IGraphContext): ISimpleGraph<TNodeAttrs, TEdgeAttrs, TGraphAttrs> {
  return {
    context,
    attrs: {} as TGraphAttrs,
    nodes: {},
    edges: {},
  };
}
