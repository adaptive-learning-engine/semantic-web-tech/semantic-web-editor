/* eslint-disable no-restricted-syntax */
async function openDir() {
  const dirHandle = await window.showDirectoryPicker();
  const fileHandle = await dirHandle.getFileHandle('supertest.txt', { create: true });
  const writable = await fileHandle.createWritable();

  // Write the contents of the file to the stream.
  await writable.write('This is text');

  // Close the file and write the contents to disk.
  await writable.close();

  dirHandle.values();
  for await (const entry of dirHandle.values()) {
    console.log(entry.kind, entry.name);
  }
}
