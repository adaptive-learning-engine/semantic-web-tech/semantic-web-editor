import { Alert, Box, Button, DialogActions, FormControlLabel, List, ListItem, MenuItem, Stack, Switch, TextField } from '@mui/material';
import React, { useState } from 'react';

import { RelationRemovalMode } from '@adlete-semantics/cass-api';
import { ICompetency } from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

import { IGraphListItem } from '../../../managers/GraphListManager';

const RELATION_REMOVAL: RelationRemovalMode[] = ['keep', 'remove', 'destroy'];

export interface IRemoveCompetencyFromFrameworkFormProps {
  framework: IGraphListItem;
  competencies: ICompetency[];
  onDisagree: () => void;
  onAgree: (competencies: ICompetency[], destroy: boolean, relationRemoval: RelationRemovalMode) => void;
}

export function RemoveCompetencyFromFrameworkForm(props: IRemoveCompetencyFromFrameworkFormProps): React.ReactElement {
  const { framework, competencies, onDisagree, onAgree } = props;

  const [doDelete, setDelete] = useState(false);
  const [relationRemoval, setRelationRemoval] = useState<RelationRemovalMode>('destroy');

  const handleDeleteChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDelete(event.target.checked);
  };

  const handleRelationRemovalChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setRelationRemoval(event.target.value as RelationRemovalMode);
  };

  const handleAgree = () => {
    // if competencies are deleted, relations will be too
    onAgree(competencies, doDelete, relationRemoval);
  };

  return (
    <Stack spacing={2}>
      <Box>Do you want to remove the following competencies from the framework?</Box>
      <TextField label="Framework" variant="outlined" value={framework?.label} disabled />
      {!framework && <Alert severity="error">Framework must be set!</Alert>}
      <List>{competencies?.map((comp) => <ListItem key={asString(comp['@id'])}>{asString(comp.name)}</ListItem>)}</List>
      <FormControlLabel control={<Switch checked={doDelete} onChange={handleDeleteChange} />} label="Destroy competencies?" />
      <TextField select value={relationRemoval} label="Relation Removal" onChange={handleRelationRemovalChange} disabled={doDelete}>
        {RELATION_REMOVAL.map((mode) => (
          <MenuItem key={mode} value={mode}>
            {mode}
          </MenuItem>
        ))}
      </TextField>

      <DialogActions>
        <Button onClick={onDisagree}>No</Button>
        <Button onClick={handleAgree} autoFocus>
          Yes
        </Button>
      </DialogActions>
    </Stack>
  );
}
