import { useMemo } from 'react';

import { ICompetency, IRelation, isCompetency, isRelation } from '@adlete-semantics/cass-json-ld';

import { EditorPanelController } from '../../../EditorPanelController';
import { useSelectedEdges, useSelectedNodes } from '../../../panels/graph-editor/hooks';

export function useSelectedCompetencies(controller: EditorPanelController): ICompetency[] {
  const nodes = useSelectedNodes(controller);
  return useMemo(() => nodes.filter((node) => isCompetency(node)) as ICompetency[], [nodes]);
}

export function useSelectedRelations(controller: EditorPanelController): IRelation[] {
  const edges = useSelectedEdges(controller);
  return useMemo(() => edges.filter((edge) => isRelation(edge)) as IRelation[], [edges]);
}
