import { Button } from '@mui/material';
import React from 'react';

import { ICompetency } from '@adlete-semantics/cass-json-ld';

import { EditorPanelController } from '../../../EditorPanelController';
import { DialogWithTitle } from '../../../components/DialogWithTitle';
import { useSelectedGraphListItem } from '../../../managers/hooks';
import { useDialogOpenClose } from '../../../utils/dialog';
import { CassProvider } from '../CassProvider';

import { CreateCompetencyInFrameworkForm } from './CreateCompetencyInFrameworkForm';

export interface ICreateCompetencyInFrameworkButtonProps {
  editor: EditorPanelController;
}

export function CreateCompetencyInFrameworkButton(props: ICreateCompetencyInFrameworkButtonProps): React.ReactElement {
  const editor = props.editor;

  const dialog = useDialogOpenClose();

  const graphListItem = useSelectedGraphListItem();

  const onSubmit = async (competency: Partial<ICompetency>) => {
    const provider = editor.semanticWebMan.providers.getProvider<CassProvider>('cass-provider');
    // TODO: support partial in createNodes
    const creationResult = await provider.createNodes([{ attrs: competency as ICompetency }]);

    if (!creationResult.every((result) => result.success)) throw new Error('Error creating Nodes!');

    const ids = creationResult.map((result) => result.id);

    await provider.addNodesToGraph(graphListItem.id, ids);
    dialog.closeDialog();
  };

  return (
    <>
      <Button onClick={dialog.openDialog} disabled={!graphListItem}>
        Create Competency In Framework
      </Button>
      <DialogWithTitle open={dialog.isOpen} handleClose={dialog.closeDialog} title="Create Competency in Framework">
        {dialog.isOpen && <CreateCompetencyInFrameworkForm framework={graphListItem} onSubmit={onSubmit} />}
      </DialogWithTitle>
    </>
  );
}
