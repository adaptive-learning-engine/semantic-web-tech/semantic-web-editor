import { Alert, Box, Button, DialogActions, FormControlLabel, List, ListItem, Stack, Switch, TextField } from '@mui/material';
import React, { useState } from 'react';

import { IRelation } from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

import { IGraphListItem } from '../../../managers/GraphListManager';

export interface IRemoveRelationFromFrameworkFormProps {
  framework: IGraphListItem;
  relations: IRelation[];
  onDisagree: () => void;
  onAgree: (relations: IRelation[], destroy: boolean) => void;
}

export function RemoveRelationFromFrameworkForm(props: IRemoveRelationFromFrameworkFormProps): React.ReactElement {
  const { framework, relations, onDisagree, onAgree } = props;

  const [doDelete, setDelete] = useState(false);

  const handleDeleteChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDelete(event.target.checked);
  };

  const handleAgree = () => {
    onAgree(relations, doDelete);
  };

  // TODO: show names of competencies
  return (
    <Stack spacing={2}>
      <Box>Do you want to remove the following relations from the framework?</Box>
      <TextField label="Framework" variant="outlined" value={framework?.label} disabled />
      {!framework && <Alert severity="error">Framework must be set!</Alert>}
      <List>{relations?.map((relation) => <ListItem key={asString(relation['@id'])}>{asString(relation['@id'])}</ListItem>)}</List>
      <FormControlLabel control={<Switch checked={doDelete} onChange={handleDeleteChange} />} label="Destroy relations?" />

      <DialogActions>
        <Button onClick={onDisagree}>No</Button>
        <Button onClick={handleAgree} autoFocus>
          Yes
        </Button>
      </DialogActions>
    </Stack>
  );
}
