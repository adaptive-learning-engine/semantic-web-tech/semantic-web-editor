import { Button } from '@mui/material';
import React from 'react';

import { getIds, ICompetency } from '@adlete-semantics/cass-json-ld';

import { EditorPanelController } from '../../../EditorPanelController';
import { DialogWithTitle } from '../../../components/DialogWithTitle';
import { useSelectedGraphListItem } from '../../../managers/hooks';
import { useDialogOpenClose } from '../../../utils/dialog';
import { CassProvider } from '../CassProvider';

import { RemoveRelationFromFrameworkForm } from './RemoveRelationFromFrameworkForm';
import { useSelectedRelations } from './hooks';

export interface IRemoveRelationFromFrameworkButtonProps {
  editor: EditorPanelController;
}

export function RemoveRelationFromFrameworkButton(props: IRemoveRelationFromFrameworkButtonProps): React.ReactElement {
  const editor = props.editor;

  const dialog = useDialogOpenClose();

  const graphListItem = useSelectedGraphListItem();

  const relations = useSelectedRelations(editor);

  const onAgree = async (_relations: ICompetency[], destroy: boolean) => {
    const provider = editor.semanticWebMan.providers.getProvider<CassProvider>('cass-provider');
    const relationIds = getIds(_relations);

    const results = destroy ? await provider.destroyEdges(relationIds) : await provider.removeEdgesFromGraph(graphListItem.id, relationIds);

    if (!results.every((result) => result.success)) throw new Error('Error removing edges!');

    dialog.closeDialog();
  };

  return (
    <>
      <Button onClick={dialog.openDialog} disabled={!graphListItem || relations?.length < 1}>
        Remove Relation From Framework
      </Button>
      <DialogWithTitle open={dialog.isOpen} handleClose={dialog.closeDialog} title="Remove Relation From Framework?">
        {dialog.isOpen && (
          <RemoveRelationFromFrameworkForm
            framework={graphListItem}
            relations={relations}
            onDisagree={dialog.closeDialog}
            onAgree={onAgree}
          />
        )}
      </DialogWithTitle>
    </>
  );
}
