import { Alert, Button, FormControl, FormControlLabel, InputLabel, MenuItem, Stack, Switch, TextField } from '@mui/material';
import React, { useState } from 'react';

import { getIdWithoutVersion, ICompetency, IRelation } from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

import { IGraphListItem } from '../../../managers/GraphListManager';

export interface ICreateRelationInFrameworkFormProps {
  framework: IGraphListItem;
  competencies: ICompetency[];
  onSubmit: (relation: Partial<IRelation>, competencyAddition: boolean) => void;
}

const RELATION_TYPES = ['desires', 'isEquivalentTo', 'narrows', 'requires'] as const;
type RelationType = (typeof RELATION_TYPES)[number];

export function CreateRelationInFrameworkForm(props: ICreateRelationInFrameworkFormProps): React.ReactElement {
  const { framework, competencies, onSubmit } = props;

  const [relationType, setRelationType] = useState<RelationType>('requires');
  const [competencyAddition, setCompetencyAddition] = useState(true);
  const [sourceTargetFlipped, setSourceTargetFlipped] = useState(false);

  const sourceIndex = sourceTargetFlipped ? 1 : 0;
  const targetIndex = sourceTargetFlipped ? 0 : 1;

  const sourceLabel = competencies?.[sourceIndex] && asString(competencies[sourceIndex].name);
  const targetLabel = competencies?.[targetIndex] && asString(competencies[targetIndex].name);

  const handleRelationTypeChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setRelationType(event.target.value as RelationType);
  };

  const handleFlipChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSourceTargetFlipped(event.target.checked);
  };

  const handleCompetencyAddition = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCompetencyAddition(event.target.checked);
  };

  const handleSubmit = () => {
    const source = getIdWithoutVersion(competencies[sourceIndex]);
    const target = getIdWithoutVersion(competencies[targetIndex]);
    onSubmit({ relationType, source, target }, competencyAddition);
  };

  return (
    <FormControl fullWidth>
      <Stack spacing={4}>
        <TextField label="Framework" variant="outlined" value={framework?.label} disabled />
        {!framework && <Alert severity="error">Framework must be set!</Alert>}
        <TextField label="Source" variant="outlined" value={sourceLabel} disabled />
        <TextField label="Target" variant="outlined" value={targetLabel} disabled />
        {competencies?.length !== 2 && <Alert severity="error">Need exactly 2 competencies for a new relation</Alert>}
        <FormControlLabel control={<Switch checked={sourceTargetFlipped} onChange={handleFlipChange} />} label="Flip Source and Target" />
        <FormControlLabel control={<Switch checked={competencyAddition} onChange={handleCompetencyAddition} />} label="Add competencies" />
        {/* <TextField label="Name" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} /> */}
        <InputLabel id="test-select-label">Label</InputLabel>
        <TextField select value={relationType} label="Relation Type" onChange={handleRelationTypeChange}>
          {RELATION_TYPES.map((type) => (
            <MenuItem key={type} value={type}>
              {type}
            </MenuItem>
          ))}
        </TextField>
        <Button onClick={handleSubmit} disabled={!framework || competencies?.length !== 2}>
          Submit
        </Button>
      </Stack>
    </FormControl>
  );
}
