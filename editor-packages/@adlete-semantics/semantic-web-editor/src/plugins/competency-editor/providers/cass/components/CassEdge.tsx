import React from 'react';
import { BaseEdge, BezierEdgeProps, getBezierPath, Position } from 'reactflow';

import { IRelation } from '@adlete-semantics/cass-json-ld';

function CassEdgeInternal({
  sourceX,
  sourceY,
  data,
  targetX,
  targetY,
  sourcePosition = Position.Bottom,
  targetPosition = Position.Top,
  labelStyle,
  labelShowBg,
  labelBgStyle,
  labelBgPadding,
  labelBgBorderRadius,
  style,
  markerEnd,
  markerStart,
  pathOptions,
  interactionWidth, // TODO: remove label from props
}: BezierEdgeProps<IRelation>) {
  const [path, labelX, labelY] = getBezierPath({
    sourceX,
    sourceY,
    sourcePosition,
    targetX,
    targetY,
    targetPosition,
    curvature: pathOptions?.curvature,
  });

  const label = data.relationType;

  return (
    <BaseEdge
      path={path}
      labelX={labelX}
      labelY={labelY}
      label={label}
      labelStyle={labelStyle}
      labelShowBg={labelShowBg}
      labelBgStyle={labelBgStyle}
      labelBgPadding={labelBgPadding}
      labelBgBorderRadius={labelBgBorderRadius}
      style={style}
      markerEnd={markerEnd}
      markerStart={markerStart}
      interactionWidth={interactionWidth}
    />
  );
}

CassEdgeInternal.displayName = 'BezierEdge';

export const CassEdge = React.memo(CassEdgeInternal);
