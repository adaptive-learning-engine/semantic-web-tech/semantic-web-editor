import { Button } from '@mui/material';
import React from 'react';

import { RelationRemovalMode } from '@adlete-semantics/cass-api';
import { getIds, ICompetency } from '@adlete-semantics/cass-json-ld';

import { EditorPanelController } from '../../../EditorPanelController';
import { DialogWithTitle } from '../../../components/DialogWithTitle';
import { useSelectedGraphListItem } from '../../../managers/hooks';
import { useDialogOpenClose } from '../../../utils/dialog';
import { CassProvider } from '../CassProvider';

import { RemoveCompetencyFromFrameworkForm } from './RemoveCompetencyFromFrameworkForm';
import { useSelectedCompetencies } from './hooks';

export interface IRemoveCompetencyFromFrameworkButtonProps {
  editor: EditorPanelController;
}

export function RemoveCompetencyFromFrameworkButton(props: IRemoveCompetencyFromFrameworkButtonProps): React.ReactElement {
  const editor = props.editor;

  const dialog = useDialogOpenClose();

  const graphListItem = useSelectedGraphListItem();

  const competencies = useSelectedCompetencies(editor);

  const onAgree = async (_competencies: ICompetency[], destroy: boolean, relationRemoval: RelationRemovalMode) => {
    const provider = editor.semanticWebMan.providers.getProvider<CassProvider>('cass-provider');
    const competencyIds = getIds(_competencies);

    const results = destroy
      ? await provider.destroyNodes(competencyIds)
      : await provider.removeNodesFromGraph(graphListItem.id, competencyIds, { relationRemoval });

    if (!results.every((result) => result.success)) throw new Error('Error removing nodes!');

    dialog.closeDialog();
  };

  return (
    <>
      <Button onClick={dialog.openDialog} disabled={!graphListItem || competencies?.length < 1}>
        Remove Competency From Framework
      </Button>
      <DialogWithTitle open={dialog.isOpen} handleClose={dialog.closeDialog} title="Remove Competency From Framework?">
        {dialog.isOpen && (
          <RemoveCompetencyFromFrameworkForm
            framework={graphListItem}
            competencies={competencies}
            onDisagree={dialog.closeDialog}
            onAgree={onAgree}
          />
        )}
      </DialogWithTitle>
    </>
  );
}
