/* eslint-disable @typescript-eslint/no-unused-vars */
import { addCompetenciesToFramework, addRelationsToFramework, API as CassAPI, RelationRemovalMode } from '@adlete-semantics/cass-api';
import { cassObjectsToGenericGraph, ICassGraphAttributes } from '@adlete-semantics/cass-graph';
import { cassObjectCollectionToMapObj, ICompetency, IRelation } from '@adlete-semantics/cass-json-ld';
import { asString, nodeListToRecord } from '@adlete-semantics/json-ld';
import { StatefulEventEmitter } from '@adlete-utils/events';

import {
  ICreateEdgeDatum,
  ICreateEdgeResult,
  ICreateNodeDatum,
  ICreateNodeResult,
  IExistsResult,
  IGetResult,
  ISemanticWebProvider,
  ISemanticWebProviderEvents,
  ISuccessResult,
  IUpdateDatum,
} from '../../ISemanticWebProvider';
import { createEmptySimpleGraph, ISimpleGraph, ISimpleGraphEdge } from '../../SimpleGraph';

import { ICassSimpleGraphConstructor } from './graph-utils';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ICassOptions {
  relationRemoval?: RelationRemovalMode;
  competencyAddition?: boolean;
}

export class CassProvider
  extends StatefulEventEmitter<ISemanticWebProviderEvents>
  implements ISemanticWebProvider<ICompetency, IRelation, ICassGraphAttributes, ICassOptions>
{
  protected api: CassAPI;

  public readonly id = 'cass-provider';

  public constructor(api: CassAPI) {
    super();
    this.api = api;
  }

  public async createNodes(data: ICreateNodeDatum<ICompetency>[], options?: ICassOptions): Promise<ICreateNodeResult<ICompetency>[]> {
    const competencies = data.map((d) => this.api.createCompetencyObject(d.attrs));
    await this.api.postNodes(competencies);
    const result = competencies.map((competency) => ({ id: asString(competency['@id']), attrs: competency, success: true }));
    return result;
  }

  public async nodesExist(ids: string[], options?: ICassOptions): Promise<IExistsResult[]> {
    throw new Error('Method not implemented.');
  }

  public async getNodes(ids: string[], options?: ICassOptions): Promise<IGetResult<ICompetency>[]> {
    throw new Error('Method not implemented.');
  }

  public async updateNodes(data: IUpdateDatum<ICompetency>[], options?: ICassOptions): Promise<ISuccessResult[]> {
    throw new Error('Method not implemented.');
  }

  public async destroyNodes(ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    // TODO: remove frameworks that reference competency
    await this.api.deleteCompetenciesById(ids, { removeFromFrameworks: true, deleteRelations: true });
    return ids.map((id) => ({ id, success: true }));
  }

  public async createEdges(data: ICreateEdgeDatum<IRelation>[], options?: ICassOptions): Promise<ICreateEdgeResult<IRelation>[]> {
    const relations = data.map((d) => this.api.createRelationObject(d.attrs));
    await this.api.postNodes(relations);
    const result = relations.map((relation) => ({
      id: asString(relation['@id']),
      source: relation.source,
      target: relation.target,
      attrs: relation,
      success: true,
    }));
    return result;
  }

  public async edgesExist(ids: string[], options?: ICassOptions): Promise<IExistsResult[]> {
    throw new Error('Method not implemented.');
  }

  public async getEdges(ids: string[], options?: ICassOptions): Promise<IGetResult<IRelation>[]> {
    throw new Error('Method not implemented.');
  }

  public async updateEdges(data: IUpdateDatum<IRelation>[], attr: IRelation, options?: ICassOptions): Promise<ISuccessResult[]> {
    throw new Error('Method not implemented.');
  }

  public async destroyEdges(ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    await this.api.deleteRelationsById(ids, { removeFromFrameworks: true });
    return ids.map((id) => ({ id, success: true }));
  }

  public async addNodesToGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    const framework = await this.api.fetchFramework(graphId);
    const newFramework = addCompetenciesToFramework(framework, ids);
    await this.api.postNode(newFramework);
    return ids.map((id) => ({ id, success: true }));
  }

  public async nodesExistOnGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<IExistsResult[]> {
    throw new Error('Method not implemented.');
  }

  public async removeNodesFromGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    await this.api.removeCompetenciesFromFramework(graphId, ids, { relationRemoval: options.relationRemoval });
    return ids.map((id) => ({ id, success: true }));
  }

  public async addEdgesToGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    await this.api.addRelationsToFramework(graphId, ids, { competencyAddition: options?.competencyAddition });
    return ids.map((id) => ({ id, success: true }));
  }

  public async edgesExistOnGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<IExistsResult[]> {
    throw new Error('Method not implemented.');
  }

  public async removeEdgesFromGraph(graphId: string, ids: string[], options?: ICassOptions): Promise<ISuccessResult[]> {
    await this.api.removeRelationsFromFramework(graphId, ids);
    return ids.map((id) => ({ id, success: true }));
  }

  public async getGraph(id: string, options?: ICassOptions): Promise<ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>> {
    const collection = await this.api.fetchFullFrameworks([id]);
    const objects = cassObjectCollectionToMapObj(collection);

    const graph = createEmptySimpleGraph<ICompetency, IRelation, ICassGraphAttributes>({ id, providerId: 'cass-provider' });
    cassObjectsToGenericGraph(objects, new ICassSimpleGraphConstructor(graph));
    return graph;
  }

  public async getGraphIds(options?: ICassOptions): Promise<string[]> {
    throw new Error('Method not implemented.');
  }
}
