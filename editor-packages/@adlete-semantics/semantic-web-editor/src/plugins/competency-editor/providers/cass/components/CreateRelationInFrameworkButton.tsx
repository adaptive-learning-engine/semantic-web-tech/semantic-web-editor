import { Button } from '@mui/material';
import React from 'react';

import { IRelation } from '@adlete-semantics/cass-json-ld';

import { EditorPanelController } from '../../../EditorPanelController';
import { DialogWithTitle } from '../../../components/DialogWithTitle';
import { useSelectedGraphListItem } from '../../../managers/hooks';
import { useDialogOpenClose } from '../../../utils/dialog';
import { CassProvider } from '../CassProvider';

import { CreateRelationInFrameworkForm } from './CreateRelationInFrameworkForm';
import { useSelectedCompetencies } from './hooks';

export interface ICreateRelationInFrameworkButtonProps {
  editor: EditorPanelController;
}

export function CreateRelationInFrameworkButton(props: ICreateRelationInFrameworkButtonProps): React.ReactElement {
  const editor = props.editor;

  const dialog = useDialogOpenClose();

  const graphListItem = useSelectedGraphListItem();
  const competencies = useSelectedCompetencies(editor);

  const onSubmit = async (relation: Partial<IRelation>, competencyAddition: boolean) => {
    const provider = editor.semanticWebMan.providers.getProvider<CassProvider>('cass-provider');
    // TODO: support partial in createEdges
    const creationResult = await provider.createEdges([{ source: relation.source, target: relation.target, attrs: relation as IRelation }]);

    if (!creationResult.every((result) => result.success)) throw new Error('Error creating edges!');

    const ids = creationResult.map((result) => result.id);

    await provider.addEdgesToGraph(graphListItem.id, ids, { competencyAddition });
    dialog.closeDialog();
  };

  return (
    <>
      <Button onClick={dialog.openDialog} disabled={!graphListItem || competencies.length !== 2}>
        Create Relation In Framework
      </Button>
      <DialogWithTitle open={dialog.isOpen} handleClose={dialog.closeDialog} title="Create Relation in Framework">
        {dialog.isOpen && <CreateRelationInFrameworkForm framework={graphListItem} competencies={competencies} onSubmit={onSubmit} />}
      </DialogWithTitle>
    </>
  );
}
