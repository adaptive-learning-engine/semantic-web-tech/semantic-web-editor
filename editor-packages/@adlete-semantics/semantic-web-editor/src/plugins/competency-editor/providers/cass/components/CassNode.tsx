import React, { memo } from 'react';
import { Handle, NodeProps, Position } from 'reactflow';

// TODO: remove /index
import type { IWithName } from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

function CassNodeInternal({ data, isConnectable, targetPosition = Position.Top, sourcePosition = Position.Bottom }: NodeProps<IWithName>) {
  return (
    <>
      <Handle type="target" position={targetPosition} isConnectable={isConnectable} />
      {data && asString(data.name)}
      <Handle type="source" position={sourcePosition} isConnectable={isConnectable} />
    </>
  );
}

CassNodeInternal.displayName = 'CassNode';

export const CassNode = memo(CassNodeInternal);
