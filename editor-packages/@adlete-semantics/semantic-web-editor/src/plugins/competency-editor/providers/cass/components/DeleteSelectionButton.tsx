import { Button } from '@mui/material';
import React, { useMemo } from 'react';

import { asString } from '@adlete-semantics/json-ld';
import { useStatefulEvent } from '@adlete-utils/events-react';

import { EditorPanelController } from '../../../EditorPanelController';
import { CassProvider } from '../CassProvider';

export interface IDeleteSelectionButtonProps {
  editor: EditorPanelController;
}

export function DeleteSelectionButton(props: IDeleteSelectionButtonProps): React.ReactElement {
  const editor = props.editor;

  const cassProvider = useMemo(() => editor.semanticWebMan.providers.getProvider<CassProvider>('cass-provider'), [editor]);

  const graph = useStatefulEvent(editor.events, 'graph');
  const selection = useStatefulEvent(editor.events, 'selection');

  async function deleteSelection() {
    // console.log(selection);
    if (selection?.nodes?.[0]) {
      const nodeId = selection?.nodes?.[0].id;
      await cassProvider.removeNodesFromGraph(asString(graph.attrs['@id']), [nodeId]);
      await cassProvider.destroyNodes([nodeId]);
    }
  }

  return <Button onClick={deleteSelection}>Delete Competency From Framework</Button>;
}
