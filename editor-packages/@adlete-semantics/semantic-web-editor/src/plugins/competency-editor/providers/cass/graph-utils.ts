import { ICassGraphAttributes, IGraphConstructor } from '@adlete-semantics/cass-graph';
import { ICompetency, IRelation } from '@adlete-semantics/cass-json-ld';

import { ISimpleGraph, ISimpleGraphEdge } from '../../SimpleGraph';

export class ICassSimpleGraphConstructor implements IGraphConstructor<ICompetency, IRelation, ICassGraphAttributes> {
  public graph: ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>;

  public constructor(graph: ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>) {
    this.graph = graph;
  }

  addNode(node: unknown, attributes?: ICompetency): void {
    this.graph.nodes[String(node)] = attributes;
  }

  addDirectedEdgeWithKey(edge: unknown, source: unknown, target: unknown, attributes?: IRelation): void {
    const edgeObj: ISimpleGraphEdge<IRelation> = {
      source: String(source),
      target: String(target),
      attrs: attributes,
    };
    this.graph.edges[String(edge)] = edgeObj;
  }

  setAttribute<AttributeName extends keyof ICassGraphAttributes>(name: AttributeName, value: ICassGraphAttributes[AttributeName]): void {
    this.graph.attrs[name] = value;
  }
}

export function mergeGraphs(
  graphs: ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes>[]
): ISimpleGraph<ICompetency, IRelation, ICassGraphAttributes> {
  const nodes: Record<string, ICompetency> = {};
  const edges: Record<string, ISimpleGraphEdge<IRelation>> = {};

  graphs.forEach((graph) => {
    // TODO: handle placeholders
    Object.entries(graph.nodes).forEach(([id, nodeAttrs]) => {
      if (!(id in nodes)) nodes[id] = nodeAttrs;
    });

    Object.entries(graph.edges).forEach(([id, edgeAttrs]) => {
      if (!(id in edges)) edges[id] = edgeAttrs;
    });
  });

  return {
    context: graphs[0].context, // TODO: merge contexts
    attrs: graphs[0].attrs, // TODO: merge attrs
    nodes,
    edges,
  };
}
