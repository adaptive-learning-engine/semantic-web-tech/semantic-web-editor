import { Alert, Button, Stack, TextField } from '@mui/material';
import React, { useState } from 'react';

import { ICompetency } from '@adlete-semantics/cass-json-ld';

import { IGraphListItem } from '../../../managers/GraphListManager';

export interface ICreateCompetencyInFrameworkFormProps {
  framework: IGraphListItem;
  onSubmit: (competency: Partial<ICompetency>) => void;
}

export function CreateCompetencyInFrameworkForm(props: ICreateCompetencyInFrameworkFormProps): React.ReactElement {
  const { framework, onSubmit } = props;

  const [name, setName] = useState('');

  const handleSubmit = () => {
    onSubmit({ name: { '@language': 'en-us', '@value': name } });
  };

  return (
    <Stack spacing={2}>
      <TextField label="Framework" variant="outlined" value={framework?.label} disabled />
      {!framework && <Alert severity="error">Framework must be set!</Alert>}
      <TextField label="Name" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
      <Button onClick={handleSubmit} disabled={!framework}>
        Submit
      </Button>
    </Stack>
  );
}
