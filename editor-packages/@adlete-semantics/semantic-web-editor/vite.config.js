import path from 'path';

import dotenv from 'dotenv';

import { defineConfig } from 'vite';
import reactPlugin from '@vitejs/plugin-react';
// import bundleVisualizer from 'rollup-plugin-visualizer';
// import analyze from 'rollup-plugin-analyzer';

// const EMPTY_FILE = 'export default {}';
// const EMPTY_FILE_NAME = '\0rollup_plugin_ignore_empty_module_placeholder';

// function ignoreModules(list) {
//   return {
//     enforce: 'pre',
//     resolveId(importee) {
//       return importee === EMPTY_FILE_NAME || list.includes(importee) ? EMPTY_FILE_NAME : null;
//     },
//     load(id) {
//       return id === EMPTY_FILE_NAME ? EMPTY_FILE : null;
//     },
//   };
// }

// load .env-File into environment variables
dotenv.config();

const { COMPETENCY_EDITOR_PORT } = process.env;

const port = COMPETENCY_EDITOR_PORT || 3001;

const aliasPackages = [
  '@adlete/visualizer-core',
  '@adlete-semantics/cass-api',
  '@adlete-semantics/cass-graph',
  '@adlete-semantics/cass-json-ld',
  '@adlete-semantics/json-ld',
  '@adlete-utils/events',
  '@adlete-utils/events-react',
  '@adlete-vle/utils',
  '@fki/editor-core',
  '@fki/plugin-based-app',
  '@fki/plugin-system',
  'graphology-sugiyama',
];

function addAliasPackages(aliases, aliasPackages) {
  aliasPackages.forEach((pkg) => {
    aliases[pkg] = path.join(path.dirname(require.resolve(`${pkg}/package.json`)), 'src');
  });
}

const alias = {};
addAliasPackages(alias, aliasPackages);

// https://vitejs.dev/config/
export default defineConfig({
  base: './',

  // root: path.join(__dirname, 'public'),
  build: {
    commonjsOptions: {
      transformMixedEsModules: true,
    },
    // these are the minimum versions supporting top-level-await
    target: ['chrome89', 'edge89', 'firefox89', 'safari15'], // es2021
    rollupOptions: {
      input: {
        main: path.join(__dirname, 'index.html'),
        // editor: path.join(__dirname, 'editor.html'),
      },
    },
    sourcemap: true,
  },
  plugins: [
    reactPlugin(),
    // ignoreModules(['plotly.js-dist-min']),
    // bundleVisualizer({
    //   emitFile: true,
    //   file: './dist/stats.html',
    // }),
    // analyze({ summaryOnly: true, limit: 50 }),
  ],
  resolve: {
    // resolve these modules directly from typescript source code (not builds!)
    alias: alias,
  },
  server: {
    port: port,
  },
});
