# @adlete-semantics/semantic-web-editor

Editor for the semantic web

## TODOs

### Semantic Web

- [ ] Create a minimal version of `Provider.getGraph` that only returns node ids and edge source and target ids
  - how to display additional information, e.g. label?
- [ ] Functions for checking equality of web resources (e.g. CASS URLs)
- [ ] Handle cases like:
  - sometimes edges are semantic web resources themselves (e.g. CASS)
  - sometimes edges are not semantic web resources (e.g. OERSI `teaches`)
  - sometimes node data should not be loaded (e.g. learning resources)
  - sometimes nodes are not semantic web resources, but part of a web resource (e.g. RDF)

### Competencies
